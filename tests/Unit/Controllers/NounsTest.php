<?php

namespace Tests\Unit\Controllers;

use App\Models\Noun;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\Response;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

// todo: Useful test info: https://matthewdaly.co.uk/blog/2018/02/25/unit-testing-your-laravel-controllers/
// these tests owe much to this article: https://auth0.com/blog/testing-laravel-apis-with-phpunit/
class NounsTest extends TestCase
{
    use RefreshDatabase;
    use WithoutMiddleware;

    public function setUp(): void
    {
        parent::setUp();
        // todo: should have tests which involve middleware.
        // See: https://stackoverflow.com/questions/46769959/laravel-phpunit-test-with-api-token-authentication
        $this->withoutMiddleware();
    }

    /** @test */
    public function testIndexReturnsDataInValidFormat(): void
    {
        $this->json('get', 'api/nouns')
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
                '*' => ['english', 'russian', 'gender', 'notes']
            ]);
    }

    /** @test */
    public function testNounIsCreatedSuccessfully(): void
    {
        $payload = [
            'english' => $this->faker->text(30),
            'russian' => $this->faker->text(20),
            'gender' => 'f',
            'notes' => $this->faker->text(100),
        ];

        $this->json('post', 'api/nouns', $payload)
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
                'english', 'russian', 'gender', 'notes'
            ]);

        $this->assertDatabaseHas('nouns', $payload);
    }

    /** @test */
    public function testSingleNounIsShownCorrectly(): void
    {
        $noun = Noun::query()->create([
            'english' => $this->faker->text(30),
            'russian' => $this->faker->text(20),
            'gender' => 'f',
            'notes' => $this->faker->text(100),
        ]);

        $this->json('get', 'api/nouns/' . $noun->id)
            ->assertStatus(Response::HTTP_OK)
            ->assertExactJson([
                'id' => $noun->id,
                'english' => $noun->english,
                'russian' => $noun->russian,
                'gender' => $noun->gender,
                'notes' => $noun->notes,
                'created_at' => $noun->created_at,
                'updated_at' => $noun->updated_at,
            ]);
    }

    /** @test */
    public function testNounCanBeDeleted(): void
    {
        $nounData = [
            'english' => $this->faker->text(30),
            'russian' => $this->faker->text(20),
            'gender' => 'f',
            'notes' => $this->faker->text(100),
        ];

        $noun = Noun::query()->create($nounData);

        $this->json('delete', 'api/nouns/' . $noun->id)
            ->assertStatus(Response::HTTP_OK)
            ->assertExactJson([]);

        $this->assertDatabaseMissing('nouns', $nounData);
    }

    /** @test */
    public function testUpdateNounReturnsCorrectData(): void
    {
        $noun = Noun::query()->create([
            'english' => $this->faker->text(30),
            'russian' => $this->faker->text(20),
            'gender' => 'm',
            'notes' => $this->faker->text(100),
        ]);

        $payload = [
            'english' => $this->faker->text(30),
            'russian' => $this->faker->text(20),
            'gender' => 'f',
            'notes' => $this->faker->text(100),
        ];

        $this->json('put', 'api/nouns/' . $noun->id, $payload)
            ->assertStatus(Response::HTTP_OK)
            ->assertExactJson([
                'id' => $noun->id,
                'english' => $payload['english'],
                'russian' => $payload['russian'],
                'gender' => 'f',
                'notes' => $payload['notes'],
                'created_at' => $noun->created_at,
                'updated_at' => $noun->updated_at,
            ]);
    }

    // todo: Test to handle duplicate entry (key: russian)
}
