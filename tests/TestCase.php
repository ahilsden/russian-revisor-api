<?php

namespace Tests;

use Faker\Factory;
use Faker\Generator;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Artisan;
use PHPUnit\Framework\Exception;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseMigrations;

    protected Generator $faker;

    public function setUp(): void {

        parent::setUp();

        $this->faker = Factory::create();

        Artisan::call('migrate:refresh');
    }

    public function __get(string $key): Generator
    {

        if ($key === 'faker') {
            return $this->faker;
        }

        throw new Exception('Unknown Key Requested');
    }
}
