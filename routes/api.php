<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\Nouns;
use App\Http\Middleware\CheckJWT;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('nouns', Nouns::class)->only(['index', 'show']);

Route::resource('nouns', Nouns::class)->only(['store', 'update', 'destroy'])
    ->only(['store', 'update', 'destroy'])
    ->middleware(CheckJWT::class);
