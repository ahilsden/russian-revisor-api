<?php

namespace Database\Factories;

use App\Models\Noun;
use Illuminate\Database\Eloquent\Factories\Factory;

class NounFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Noun::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'english' => $this->faker->text(20),
            'russian' => $this->faker->text(20),
            'gender' => $this->faker->text(5),
            'notes' => $this->faker->text(100),
        ];
    }
}
