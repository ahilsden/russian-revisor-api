<?php

namespace Database\Seeders;

use App\Models\Noun;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Noun::factory()
            ->times(3)
            ->create();
    }
}
