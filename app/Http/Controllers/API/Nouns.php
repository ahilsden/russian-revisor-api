<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Noun;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class Nouns extends Controller
{
    private string $english;

    public function index(): JsonResponse
    {
        $nouns = Noun::all();

        return response()->json($nouns);
    }

    public function store(Request $request): JsonResponse
    {
        // todo: handle duplicate entry (key: russian)
        $request->validate([
            'english' => 'required|max:30',
            'russian' => 'required|max:30',
            'gender' => 'required|max:1',
            'notes' => 'max:100',
        ]);

        $noun = new Noun([
            'english' => $request->get('english'),
            'russian' => $request->get('russian'),
            'gender' => $request->get('gender'),
            'notes' => $request->get('notes'),
        ]);

        $noun->save();

        return response()->json($noun);
    }


    public function show(int $id): JsonResponse
    {
        $noun = Noun::query()->findOrFail($id);

        return response()->json($noun);
    }

    public function update(Request $request, $id)
    {
        // todo: handle duplicate entry (key: russian)
        $noun = Noun::query()->findOrFail($id);

        $request->validate([
            'english' => 'required|max:30',
            'russian' => 'required|max:30',
            'gender' => 'required|max:1',
            'notes' => 'max:100',
        ]);

        $noun->english = $request->get('english');
        $noun->russian = $request->get('russian');
        $noun->gender = $request->get('gender');
        $noun->notes = $request->get('notes');

        $noun->save();

        return response()->json($noun);
    }

    public function destroy(int $id): JsonResponse
    {
        $noun = Noun::query()->findOrFail($id);
        $noun->delete();

        return response()->json($noun::all());
    }
}
